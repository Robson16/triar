<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main>
	<section class="container-fluid">
		<div class="error-message">
			<span>404</span>
			<h1 class="page-title"><?php _e("Page not found", 'triar'); ?></h1>
			<p><?php _e('Could not find the page you are looking for.', 'triar'); ?></p>
			<a href="<?php echo get_home_url(); ?>"><?php _e('Home page', 'triar'); ?></a>
		</div>
	</section>
</main>

<?php
get_footer();
