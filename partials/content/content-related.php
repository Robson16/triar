<?php

/**
 * The template for displaying related posts
 */
?>

<div class="related">
	<?php
	$tags = wp_get_post_tags($post->ID);
	$tags_id = array_column($tags, 'term_id');

	if ($tags) :
	?>
		<h4 class="related-title"> <?php _e('Related Posts', 'triar'); ?> </h4>
		<div class="related-grid">
			<?php
			$my_query = new WP_Query(array(
				'tag__in'				=> $tags_id,
				'post__not_in'			=> [$post->ID],
				'posts_per_page'		=> 3,
				'ignore_sticky_posts'	=> true,
				'orderby'				=> 'rand',
			));

			if ($my_query->have_posts()) {
				while ($my_query->have_posts()) {
					$my_query->the_post();
					get_template_part('partials/content/content', 'excerpt');
				}
				wp_reset_query();
			}
			?>
		</div>
		<!-- /.related-grid -->
	<?php endif; ?>
</div>
<!-- /.related-posts -->
