<?php

/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('post-excerpt'); ?>>
	<header class="entry-header">

		<a class="post-thumbnail" href="<?php echo esc_url(get_permalink()); ?>">
			<?php
			the_post_thumbnail('thumbnail', array('title' => get_the_title()));
			the_title('<span class="screen-reader-text">', '</span>');
			?>
		</a>

		<?php

		if (is_sticky() && is_home() && !is_paged()) {
			printf('<span class="sticky-post">%s</span>', _x('Post', 'Featured', 'triar'));
		}

		the_title(sprintf('<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h3>');

		?>

	</header>
	<!-- /.entry-header -->

	<footer class="entry-footer">
		<a class="read-more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php _e('Read more', 'triar') ?></a>
	</footer>
	<!-- /.entry-footer -->
</div><!-- #post-<?php the_ID(); ?> -->
