<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<header id="header">
		<nav id="navbar" class="navbar ">
			<div class="container-fluid">
				<?php
				if (has_custom_logo()) {
					the_custom_logo();
				} else {
					echo '<h1 style="margin: 0;">' . get_bloginfo('title') . '</h1>';
				}
				?>

				<button type="button" class="navbar-toggler" data-target="#navbar-nav">
					<span class="navbar-toggler-icon">
						<div class="bar1"></div>
						<div class="bar2"></div>
						<div class="bar3"></div>
					</span>
				</button>

				<?php
				wp_nav_menu(array(
					'theme_location' => 'main_menu',
					'depth' => 2,
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id' => 'navbar-nav',
					'menu_class' => 'navbar-nav has-dropdown-menu-to-right',
					'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
					'walker' => new WP_Bootstrap_Navwalker()
				));
				?>
			</div>
			<!-- /.container-fluid -->
		</nav>

		<?php if (!is_page() && !is_404() && !is_single()) : ?>
			<div class="header-inner" style="
				background-image: url( <?php header_image(); ?>	);
				color: <?php echo '#' . get_header_textcolor(); ?>;
			">
				<div class="container">
					<?php
					if (is_search()) {
						echo sprintf(
							'<span class="header-title">%s <span>%s</span></span>',
							__('Search results for:', 'triar'),
							get_search_query()
						);
					}

					if (is_archive()) echo sprintf('<h1 class="header-title">%s</h1>', single_term_title("", false));
					if (is_home()) echo '<h1 class="header-title">Blog</h1>';
					?>
				</div>
				<!-- /.container -->
			</div>
			<!-- /.page-header -->
		<?php endif; ?>
	</header>
