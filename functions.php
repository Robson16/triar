<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Front-End
function triar_scripts()
{
	// CSS
	wp_enqueue_style('triar-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('triar-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('triar-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('triar-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'triar_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function triar_gutenberg_scripts()
{
	// Web Fonts
	wp_enqueue_style('triar-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
}
add_action('enqueue_block_editor_assets', 'triar_gutenberg_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function triar_setup()
{
	// Enabling translation support
	$textdomain = 'triar';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 33,
		'width'       => 176,
		'flex-height' => false,
		'flex-width'  => false,
		'header-text' => array('site-title', 'site-description'),
	));

	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'triar'),
		'footer_menu' => __('Footer Menu', 'triar'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/editor-styles.css');

	// Add Custom Header Support
	add_theme_support('custom-header', array(
		'default-image'      => 'https://via.placeholder.com/2560x400/e7481d/ffffff',
		'default-text-color' => 'fff',
		'width'              => 2560,
		'height'             => 400,
		'flex-width'         => true,
		'flex-height'        => true,
	));

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Enables wide and full dimensions
	add_theme_support('align-wide');

	// Standard style for each block.
	add_theme_support('wp-block-styles');

	// Creates the specific color palette
	add_theme_support('editor-color-palette', array(
		array(
			'name'  => __('Black', 'triar'),
			'slug'  => 'black',
			'color' => '#000000',
		),
		array(
			'name'  => __('Granite Gray', 'triar'),
			'slug'  => 'granite-gray',
			'color' => '#666666',
		),
		array(
			'name'  => __('Flame', 'triar'),
			'slug'  => 'flame',
			'color' => '#e7481d',
		),
		array(
			'name'  => __('White', 'triar'),
			'slug'  => 'white',
			'color' => '#ffffff',
		),
	));

	// Custom font sizes.
	add_theme_support('editor-font-sizes', array(
		array(
			'name' => __('Small', 'triar'),
			'size' => 16,
			'slug' => 'small',
		),
		array(
			'name' => __('Normal', 'triar'),
			'size' => 18,
			'slug' => 'normal',
		),
		array(
			'name' => __('Medium', 'triar'),
			'size' => 24,
			'slug' => 'medium',
		),
		array(
			'name' => __('Big', 'triar'),
			'size' => 40,
			'slug' => 'big',
		),
		array(
			'name' => __('Huge', 'triar'),
			'size' => 50,
			'slug' => 'huge',
		),
	));
}
add_action('after_setup_theme', 'triar_setup');

/**
 * Remove website field from comment form
 *
 */
function triar_website_remove($fields)
{
	if (isset($fields['url']))
		unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'triar_website_remove');

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';
