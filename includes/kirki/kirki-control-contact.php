<?php

new \Kirki\Section(
	'triar_section_contacts',
	array(
		'title'       => esc_html__('Contacts', 'triar'),
		'description' => esc_html__('Contact informations.', 'triar'),
		'priority'    => 160,
	)
);

new \Kirki\Field\Text(
	array(
		'settings' => 'triar_setting_phone',
		'label'    => esc_html__('Phone', 'triar'),
		'section'  => 'triar_section_contacts',
		'default'  => '',
		'priority' => 10,
	)
);

new \Kirki\Field\Text(
	array(
		'settings' => 'triar_setting_email',
		'label'    => esc_html__('E-mail', 'triar'),
		'section'  => 'triar_section_contacts',
		'default'  => '',
		'priority' => 10,
	)
);
