<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">

	<div class="container">
		<?php
		wp_nav_menu(array(
			'theme_location' => 'footer_menu',
			'depth' => 1,
			'container_class' => 'footer-menu-wrap',
			'menu_class' => 'footer-menu',
		));
		?>
	</div>
	<!-- /.container -->

	<div class="footer-middle">
		<div class="container-fluid">
			<div class="footer-logo">
				<?php
				if (has_custom_logo()) {
					the_custom_logo();
				} else {
					echo '<h1 style="margin: 0;">' . get_bloginfo('title') . '</h1>';
				}
				?>
			</div>
			<!-- /.footer-logo -->

			<div class="footer-contacts">
				<?php if (get_theme_mod("triar_setting_phone")) : ?>
					<div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/icon-phone-white.png'; ?>" alt="Icon">
						<a href="<?php echo "tel:+55" . preg_replace('/\D+/', '', get_theme_mod("triar_setting_phone")); ?>">
							<?php echo get_theme_mod("triar_setting_phone"); ?>
						</a>
					</div>
				<?php endif; ?>

				<?php if (get_theme_mod("triar_setting_email")) : ?>
					<div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/icon-mail-white.png'; ?>" alt="Icon">
						<a href="<?php echo "mailto:" . get_theme_mod("triar_setting_email"); ?>">
							<?php echo get_theme_mod("triar_setting_email"); ?>
						</a>
					</div>
				<?php endif; ?>
			</div>
			<!-- /.footer-contacts -->
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.footer-middle -->

	<div class="footer-copyright">
		<div class="container-fluid">
			<span>
				&copy;&nbsp;<?php echo bloginfo('title'); ?>&nbsp;<?php echo wp_date('Y') . '.'; ?>&nbsp;<?php _e('All rights reserved.', 'triar') ?>

				<?php _e('Developed by', 'triar') ?>
				<a href="https://agenciabeb.com.br/" target="_blank" rel="noopener">
					<img src="<?php echo get_template_directory_uri() . '/assets/images/agenciabeb.png'; ?>" alt="Agência B&B">
				</a>
			</span>
		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.footer-copyright -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
