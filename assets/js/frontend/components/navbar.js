export default class Navbar {
	constructor() {
		this.wpadminbar = document.querySelector('#wpadminbar');
		this.navbar = document.querySelector('#navbar');
		this.viwewportX = document.documentElement.clientWidth;
		this.offset = this.navbar.offsetHeight;
		this.events();
	}

	// Events Triggers
	events() {
		window.addEventListener('scroll', () => {
			this.handleWhenLoggedIn();
		});

		window.addEventListener('resize', () => {
			this.handleViwewportSize();
		});

		this.handleNavbarToggle();
	}

	handleWhenLoggedIn() {
		// When the WP Admin Bar is visible, we need to change the top space to stick the navbar menu visible below it
		if (window.scrollY > this.offset) {
			if (this.wpadminbar && this.viwewportX > 576) {
				this.navbar.style.top = this.wpadminbar.offsetHeight + 'px';
			}
		} else if (this.wpadminbar && this.viwewportX > 576) {
			this.navbar.style.top = 'initial';
		}
	}

	handleViwewportSize() {
		this.viwewportX = document.documentElement.clientWidth;
	}

	handleNavbarToggle() {
		const navbarToggler = this.navbar.querySelector('.navbar-toggler');
		const icon = navbarToggler.querySelector('.navbar-toggler-icon');
		let target = navbarToggler.dataset.target;

		target = document.querySelector(target);

		if (target) {
			navbarToggler.addEventListener('click', () => {
				target.classList.toggle('show');
				icon.classList.toggle('close');
			});
		}
	}
}
